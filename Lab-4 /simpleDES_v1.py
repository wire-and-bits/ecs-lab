def S_DES(plain_text, key):
    # Initial Permutation Table
    IP = [2, 6, 3, 1, 4, 8, 5, 7]

    # Inverse Initial Permutation Table
    IP_inv = [4, 1, 3, 5, 7, 2, 8, 6]

    # Expansion Permutation Table
    EP = [4, 1, 2, 3, 2, 3, 4, 1]

    # Permutation Table
    P4 = [2, 4, 3, 1]

    # S-boxes (Decomposed into s0 and s1)
    s0 = [[1, 0, 3, 2],
          [3, 2, 1, 0],
          [0, 2, 1, 3],
          [3, 1, 3, 2]]

    s1 = [[0, 1, 2, 3],
          [2, 0, 1, 3],
          [3, 0, 1, 0],
          [2, 1, 0, 3]]

    # Function to apply permutation
    def permute(k, arr, n):
        permutation = ""
        for i in range(0, n):
            permutation = permutation + k[arr[i] - 1]
        return permutation

    # Function to perform xor
    def xor(a, b):
        ans = ""
        for i in range(len(a)):
            if a[i] == b[i]:
                ans = ans + "0"
            else:
                ans = ans + "1"
        return ans

    # Function to perform S-box lookup
    def lookup_in_sbox(b, sbox):
        row = int(b[0] + b[3], 2)
        col = int(b[1] + b[2], 2)
        return format(sbox[row][col], '02b')

    # Function to perform f_k
    def f_k(k, SK):
        L, R = k[:4], k[4:]
        temp = permute(R, EP, 8)
        temp = xor(temp, SK)
        temp = lookup_in_sbox(temp[:4], s0) + lookup_in_sbox(temp[4:], s1)
        temp = permute(temp, P4, 4)
        return xor(temp, L), R

    # Initial Permutation
    plain_text = permute(plain_text, IP, 8)

    # Round 1
    L, R = f_k(plain_text, key)

    # Switch
    plain_text = R + L

    # Round 2
    L, R = f_k(plain_text, key)

    # Inverse Initial Permutation
    cipher_text = permute(L + R, IP_inv, 8)

    return cipher_text

# Test the function
plain_text = input("Enter Plaintext \n")  # Example plaintext
key = "1010000010"  # Example key
print("Cipher Text : ", S_DES(plain_text, key))
